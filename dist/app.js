import http from 'http';
const server = http.createServer((req, res) => {
    res.writeHead(200, 'Success', { 'Content-Type': 'text/plain' });
    res.end('Hello, World!');
});
server.listen(3000, () => {
    // eslint-disable-next-line no-console
    console.log('Listening on port 3000');
});
