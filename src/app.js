"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var https_1 = require("https");
var server = https_1.default.createServer(function (req, res) {
    res.writeHead(200, 'Success', { 'Content-Type': 'text/plain' });
    res.end('Hello, World!');
});
server.listen(3000, function () {
    // eslint-disable-next-line no-console
    console.log('Listening on port 3000');
});
